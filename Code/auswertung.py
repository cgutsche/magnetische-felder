# %%
import scipy as sp
import scipy.stats as stats
import numpy as np
import pandas as pd
import scipy.constants
import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
from uncertainties import ufloat
# %%
task1 = pd.read_csv("Messung EM felder - Messung1.csv")
# %%
dat1 = task1.iloc[3:,[0,1,2,6]]
dat1 = dat1.sort_values("T in ms")
dat1
# %%
omega = 1/(dat1["T in ms"]*1e-3) *2 * np.pi
A = dat1["2A in mV"]/2*1e-3
dat1["err_A"] = (dat1.iloc[:,1] - dat1.iloc[:,2])/2/2*1e-3
dat1["err_A"] = (dat1.iloc[:,1] - dat1.iloc[:,2])/2/2*1e-3
err_A = dat1["err_A"]
err_T = [3] * len(omega)

def plot_regression(x, y, err_y, err_x, filename):
    df_xy = pd.DataFrame({
    'x': x,
    'y': y,
    })
    # wenn ich 1/err_A**2 als gewicht nehme, dann wird der fehler riesig
    # weights = pd.Series(err_y)
    weights = 1/err_y**2
    # -1 drops the intercept
    wls_fit = sm.wls('y ~ x -1 ', data=df_xy, weights=weights).fit()
    ols_fit = sm.ols('y ~ x -1', data=df_xy).fit()

    slope1 = ufloat(wls_fit.params["x"], np.sqrt(wls_fit.normalized_cov_params["x"]["x"]))
    slope2 = ufloat(ols_fit.params["x"], np.sqrt(ols_fit.normalized_cov_params["x"]["x"]))
    print(slope1, slope2)
    fig = plt.figure()
    ax = fig.add_subplot(111, facecolor='w')

# weighted prediction
    wp, = ax.plot(
    df_xy['x'],
    wls_fit.predict(),
    "--",
    color='#e55ea2',
    lw=1.,
    alpha=1.0,
)
# unweighted prediction
    op, = ax.plot(  
    df_xy['x'],
    ols_fit.predict(),
    "k--",
    lw=1,
    alpha=1.0,
)
    leg = plt.legend(
    (op, wp),
    ('Ordinary Least Squares', 'Weighted Least Squares'),
    loc='upper left',
    fontsize=8)
    plt.xlabel("$\omega$ in Hz")
    plt.ylabel("A in V")

    plt.errorbar(x,y,err_y, err_x, fmt="o")
    plt.savefig(filename)
    return slope1

slope1 = plot_regression(omega, A, err_A, err_T, "task1.png")

# %%
n = 100
S = 1.12e-4
mu0 = scipy.constants.mu_0
H1 = slope1/(n*S*mu0)
print(H1)
# %%
# NOTE task 2: ein strich ist 20 mg
task2 = pd.read_csv("Messung EM felder - Messung2.csv")
task2
dat2 = task2.iloc[:,[1,3]]
I = dat2["I in mA"] * 1e-3
g = 9.81
F = 1e-6*g * dat2["m in mg"]
err_F = 1e-6*g * 20
err_I = 1e-3*task2["err I in mA"]
slope2 = plot_regression(I,F,err_F, err_I,"task2.png")
l = 1.95e-2
H2 = slope2/mu0/l
print(H2)
# 262815.
# %%
# NOTE delta h is negative. 
task3 = pd.read_csv("Messung EM felder - Messung3.csv")
dh = ufloat(np.mean(task3["h"])*1e-3, stats.sem(task3["h"])*1e-3)
muL = 1+0.38e-6
rho = 997
muF = muL + 2 * rho * g * dh/( mu0 * H2**2 * muL**2)
# das ist eigentlich die relative permability muF/mu0
print("dh", dh)
print("muF",muF)
# %%
